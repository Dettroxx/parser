<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportSitesFix extends Migration
{
    public function up()
    {
        Schema::table('import_offers', function (Blueprint $table) {
            $table->string('site_key')->nullable();
        }); 
    }

    public function down()
    {
        Schema::table('import_offers', function (Blueprint $table) {
            $table->dropColumn('site_key');
        });
    }
}
