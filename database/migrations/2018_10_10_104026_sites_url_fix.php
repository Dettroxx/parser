<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SitesUrlFix extends Migration
{
    public function up()
    {
        Schema::table('sites', function (Blueprint $table) {
            $table->text('base_url')->nullable();
        }); 
    }

    public function down()
    {
        Schema::table('sites', function (Blueprint $table) {
            $table->dropColumn('base_url'); 
        });
    }
}
