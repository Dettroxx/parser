<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 20);
            $table->text('path')->nullable();
            $table->text('title')->nullable();
            $table->float('stars', 3, 2)->default(0);
            $table->text('images')->nullable();
            $table->text('thumbs')->nullable();
            $table->double('price_current')->default(0);
            $table->double('price_old')->default(0);
            $table->double('price_discount')->default(0);
            $table->string('status', 255)->nullable();
            $table->integer('status_code')->unsigned()->default(0);
            $table->text('sizes')->nullable();
            $table->text('colors')->nullable();
            $table->text("content")->nullable();
            $table->boolean('parsed')->default(false);
            $table->text('url');
            $table->boolean('valid')->default(true);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP')); 

            $table->index(['id']);
            $table->index(['code']);
            $table->index(['parsed']);
            $table->index(['status_code']);
            $table->index(['valid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
