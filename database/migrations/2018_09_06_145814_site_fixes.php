<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SiteFixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sites', function (Blueprint $table) {
            $table->string('offer_tag')->nullable();
        }); 

        Schema::table('offers', function (Blueprint $table) {
            $table->string('uniq_link')->nullable();
        }); 

        Schema::table('articles', function (Blueprint $table) {
            $table->integer('char_count')->unsigned()->default(0); 
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sites', function (Blueprint $table) {
            $table->dropColumn('offer_tag'); 
        });

        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('uniq_link'); 
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn('char_count'); 
        });
    }
}
