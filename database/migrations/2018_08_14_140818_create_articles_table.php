<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Article;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->text('post_title')->nullable();
            $table->text('post_content')->nullable();
            $table->string('post_status')->nullable();
            $table->integer("inner_status")->unsigned()->default(0);
            $table->timestamp(Article::CREATED_AT)->nullable();
            $table->timestamp(Article::UPDATED_AT)->nullable();

            //Индексы
            $table->index('site_id');
            $table->index('post_id');
            $table->index('post_status');

            //Связь с сайтами
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
