<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteOfferLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_offer_links', function (Blueprint $table) {
            $table->integer('site_id')->unsigned()->index();
            $table->integer('offer_id')->unsigned()->index();

            //Связь с сайтами
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_offer_links');
    }
}
