<?php

use Illuminate\Database\Seeder;

class AdminUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_users')->delete();
        
        \DB::table('admin_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'username' => 'dettroxx',
                'password' => '$2y$10$u0ub.LEuxiMftT/Z7JC3Ne56qjwNPnIug8pyD51AdEpNF2WX59fsK',
                'name' => 'Сергей',
                'avatar' => NULL,
                'remember_token' => 'H1hyrDXuQ6KTficqmiPyOcnxxZlrZ13loYb8cFI18cRaR2O6ycuvu39ZL5CG',
                'created_at' => '2018-08-14 06:43:11',
                'updated_at' => '2018-08-14 12:42:27',
            ),
            1 => 
            array (
                'id' => 2,
                'username' => 'admin',
                'password' => '$2y$10$oRKtKKYhDWG72HdacpzpR.V0yZN9YqEB3/GYwDWlm64Q7/66CCUm.',
                'name' => 'Администратор',
                'avatar' => NULL,
                'remember_token' => 'pp3e0ldJWMS1pzCNKfuclSmwjUzq3VcKipbpnGMjJhziLEJt01xzZdhWBleh',
                'created_at' => '2018-08-14 12:44:10',
                'updated_at' => '2018-08-14 12:44:10',
            ),
        ));
        
        
    }
}