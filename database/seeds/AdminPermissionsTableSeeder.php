<?php

use Illuminate\Database\Seeder;

class AdminPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_permissions')->delete();
        
        \DB::table('admin_permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'All permission',
                'slug' => '*',
                'http_method' => '',
                'http_path' => '*',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Dashboard',
                'slug' => 'dashboard',
                'http_method' => 'GET',
                'http_path' => '/',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Login',
                'slug' => 'auth.login',
                'http_method' => '',
                'http_path' => '/auth/login
/auth/logout',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'User setting',
                'slug' => 'auth.setting',
                'http_method' => 'GET,PUT',
                'http_path' => '/auth/setting',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Auth management',
                'slug' => 'auth.management',
                'http_method' => '',
                'http_path' => '/auth/roles
/auth/permissions
/auth/menu
/auth/logs',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Настройка',
                'slug' => 'config',
                'http_method' => '',
                'http_path' => '/config*',
                'created_at' => '2018-08-14 12:45:04',
                'updated_at' => '2018-08-14 12:45:04',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Статьи',
                'slug' => 'articles',
                'http_method' => '',
                'http_path' => '/db/articles
/db/articles*',
                'created_at' => '2018-08-17 12:00:59',
                'updated_at' => '2018-08-17 13:48:44',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Офферы',
                'slug' => 'offers',
                'http_method' => '',
                'http_path' => '/db/offers
/db/offers*',
                'created_at' => '2018-08-17 12:02:50',
                'updated_at' => '2018-08-17 13:48:49',
            ),
        ));
        
        
    }
}