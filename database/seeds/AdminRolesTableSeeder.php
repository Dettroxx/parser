<?php

use Illuminate\Database\Seeder;

class AdminRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_roles')->delete();
        
        \DB::table('admin_roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Разработчик',
                'slug' => 'developer',
                'created_at' => '2018-08-14 06:43:11',
                'updated_at' => '2018-08-14 12:40:51',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Администратор',
                'slug' => 'admin',
                'created_at' => '2018-08-14 12:41:15',
                'updated_at' => '2018-08-14 12:42:52',
            ),
        ));
        
        
    }
}