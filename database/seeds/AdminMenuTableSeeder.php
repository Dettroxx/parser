<?php

use Illuminate\Database\Seeder;

class AdminMenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_menu')->delete();
        
        \DB::table('admin_menu')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => 2,
                'order' => 2,
                'title' => 'Index',
                'icon' => 'fa-bar-chart',
                'uri' => '/',
                'created_at' => NULL,
                'updated_at' => '2018-08-14 06:58:51',
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => 0,
                'order' => 1,
                'title' => 'Admin',
                'icon' => 'fa-tasks',
                'uri' => '',
                'created_at' => NULL,
                'updated_at' => '2018-08-14 06:58:51',
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => 2,
                'order' => 3,
                'title' => 'Users',
                'icon' => 'fa-users',
                'uri' => 'auth/users',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => 2,
                'order' => 4,
                'title' => 'Roles',
                'icon' => 'fa-user',
                'uri' => 'auth/roles',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => 2,
                'order' => 5,
                'title' => 'Permission',
                'icon' => 'fa-ban',
                'uri' => 'auth/permissions',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 2,
                'order' => 6,
                'title' => 'Menu',
                'icon' => 'fa-bars',
                'uri' => 'auth/menu',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => 2,
                'order' => 7,
                'title' => 'Operation log',
                'icon' => 'fa-history',
                'uri' => 'auth/logs',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => 0,
                'order' => 11,
                'title' => 'Настройка',
                'icon' => 'fa-bars',
                'uri' => NULL,
                'created_at' => '2018-08-14 06:59:54',
                'updated_at' => '2018-08-14 14:49:53',
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => 8,
                'order' => 12,
                'title' => 'Список сайтов',
                'icon' => 'fa-globe',
                'uri' => 'config/sites',
                'created_at' => '2018-08-14 07:00:10',
                'updated_at' => '2018-08-14 14:49:53',
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => 8,
                'order' => 13,
                'title' => 'Категории Amazon',
                'icon' => 'fa-amazon',
                'uri' => 'config/categories',
                'created_at' => '2018-08-14 08:03:48',
                'updated_at' => '2018-08-14 14:49:53',
            ),
            10 => 
            array (
                'id' => 11,
                'parent_id' => 8,
                'order' => 14,
                'title' => 'Одиночные офферы',
                'icon' => 'fa-shopping-cart',
                'uri' => 'config/offers',
                'created_at' => '2018-08-14 08:04:33',
                'updated_at' => '2018-08-14 14:49:53',
            ),
            11 => 
            array (
                'id' => 12,
                'parent_id' => 0,
                'order' => 8,
                'title' => 'База данных',
                'icon' => 'fa-bars',
                'uri' => NULL,
                'created_at' => '2018-08-14 12:46:16',
                'updated_at' => '2018-08-14 12:46:31',
            ),
            12 => 
            array (
                'id' => 13,
                'parent_id' => 12,
                'order' => 10,
                'title' => 'Офферы',
                'icon' => 'fa-amazon',
                'uri' => 'db/offers',
                'created_at' => '2018-08-14 12:47:07',
                'updated_at' => '2018-08-17 12:18:44',
            ),
            13 => 
            array (
                'id' => 14,
                'parent_id' => 12,
                'order' => 9,
                'title' => 'Посты',
                'icon' => 'fa-book',
                'uri' => 'db/articles',
                'created_at' => '2018-08-14 14:49:47',
                'updated_at' => '2018-08-14 14:51:33',
            ),
        ));
        
        
    }
}