@if ($items)
    <div style="border: 1px solid #e6e6e6; width: 150px; height: 150px; background: url('{{$items}}') no-repeat transparent; background-size: contain; background-position: center"></div>
@else
    <div style="border: 1px solid #e6e6e6; width: 150px; height: 150px;"></div>
@endif