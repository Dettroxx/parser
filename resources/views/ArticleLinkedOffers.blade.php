<style>
    .offer_avatar {
        width: 100px;
        height: 100px;
        background-size: cover;
    }

    .valid-0 {
        color: red;
    }

    .valid-1 {
        color: green;
    }
</style>

@if (count($items) > 0)
<table class="table table-bordered">
	<tbody>
		<tr>
			<th style="width: 10px">№</th>
            <th style="width: 50px">Изображение</th>
			<th style="max-width: 400px">Наименование</th>
			<th>Статус</th>
            <th>Валиден</th>
            <th>Дата обновления</th>
		</tr>

        @foreach ($items as $idx => $item)
            @php
                $offer = $item['offer'];
            @endphp

            <tr>
                <td>{{$idx+1}}.</td>
                <td>
                    <div class="offer_avatar" @if ($offer['images'])style="background-image: url('{{$offer['images']}}')"@endif></div>      
                </td>
                <td>
                    @if ($offer['title'])
                        <a href="{{$offer['url']}}" target="_blank">{{$offer['title']}}</a>
                    @else
                        <a href="{{$offer['url']}}" target="_blank">{{str_limit($offer['url'], $limit = 40, $end = '...')}}</a>
                    @endif
                </td>
                <td>
                    <span class="valid-{{$offer['valid']}}">
                        <b>
                            @if (empty($offer['status']))
                                Не определен
                            @else
                                {{$offer['status']}}
                            @endif
                        </b>
                    </span>
                </td>
                <td>
                    <span class="valid-{{$offer['valid']}}">
                        <b>
                            @if ($offer['valid'])
                                Да
                            @else
                                Нет
                            @endif
                        </b>
                    </span>
                </td>
                <td>
                    {{$offer['updated_at']}}
                </td>
            </tr>
        @endforeach
	</tbody>
</table>
@else
    <span style="color: red">
        Анализ офферов в статьях еще не был произведен
    </span>
@endif