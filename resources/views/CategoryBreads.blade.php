<ol class="breadcrumb">
    @foreach ($items as $key => $item)
        @if (count($items)-1 == $key)
            <li class="active">{{$item['text']}}</li>
        @else
            <li><a href="{{route('offers_cat', ['category' => $item['id']], false)}}">{{$item['text']}}</a></li>
        @endif
    @endforeach
</ol>