<li class="dd-item dd-nodrag" data-id="{{ $branch[$keyName] }}"> 
    <div class="handle-tree no-drag">
        {!! $branchCallback($branch) !!}
    </div>
    @if(isset($branch['children']))
    <ol class="dd-list">
        @foreach($branch['children'] as $branch)
            @include($branchView, $branch)
        @endforeach
    </ol>
    @endif
</li>