<?php
namespace App;
use Encore\Admin\Tree;

class CustomTree extends Tree {
    protected $nestableOptions = [
        "maxDepth" => 1
    ];

    protected $view = [
        'tree'   => 'custom_tree',
        'branch' => 'custom_branch',
    ];


    protected function script() {
        return parent::script().$this->afterScript();
    }

    protected function afterScript() {
        return <<<SCRIPT
            $('#{$this->elementId}').nestable('collapseAll');
            $('.treehidden').removeClass('treehidden');

            //Для того, чтобы открывать новые категории
            $(".tree-collapsex").on("click", function(e) {
                e.preventDefault();
                var current = $(this);
                var url = current.attr('href');
                var win = window.open(url, '_blank');
                win.focus();
                return false;
            });
SCRIPT;
    }
}