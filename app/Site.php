<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AmazonCategory;

class Site extends Model
{
    protected $fillable = [
        'name',
        'key',
        'url',
        'active',
        'import_config',
        'offer_tag',
        'fulllink',
        'start_params',
        'base_url'
    ];

    protected $casts = [
        'start_params' => 'json',
    ];

    public function category()
    {
        return $this->hasOne(Category::class);
    }
}
