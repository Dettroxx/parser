<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmazonOffer extends Model
{
    protected $fillable = [
        "size_id",
        "code",
        "url",
        "id"
    ];
    public $timestamps = false;

    //События сохранения
    public static function boot()
    {
        parent::boot();
        self::creating(function($model){ //Определяем код по URL
            $url = $model->url;
            $pattern = '/\/([A-Z0-9]{10}?)\//';
            $match = preg_match($pattern, $url, $matches);
            if ($match) {
                $code = $matches[1];
                $model->code = $code;
            } else {
                $pattern = '/%2F([A-Z0-9]{10}?)%2F/';
                $match = preg_match($pattern, $url, $matches);
                if ($match) {
                    $code = $matches[1];
                    $model->code = $code;
                } else {
                    $pattern = '/\/([A-Z0-9]{10}?)/';
                    $match = preg_match($pattern, $url, $matches); 
                    if ($match) {
                        $code = $matches[1];
                        $model->code = $code;
                    } else {
                        $pattern = '/\/([A-Z0-9]{10}?)\?/';
                        $match = preg_match($pattern, $url, $matches); 
                        if ($match) {
                            $code = $matches[1];
                            $model->code = $code;
                        } else {
                            
                        }
                    }
                }
            }
        });
    }
}
