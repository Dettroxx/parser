<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//Класс, который используется для импорта элементов
class ImportOffer extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'code', 'path', 'title', 'stars', 'images', 
        'thumbs', 'status', 'sizes', 'colors', 'content', 
        'price', 'old_price', 'url', 'valid_attr'
    ];
}
