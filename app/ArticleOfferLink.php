<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleOfferLink extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'article_id', 'offer_id'
    ];

    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id');
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }
}
