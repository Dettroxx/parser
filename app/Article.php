<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        "site_id",
        "post_id",
        "post_title",
        "post_content",
        "post_status",
        "inner_status",
        "post_date",
        "post_modified",
        'char_count',
        'videos_invalid',
        'videos_invalid_date'
    ];

    protected $casts = [
        'import_config' => 'array',
    ];

    //Список связей
    public function offers_links()
    {
        return $this->hasMany(ArticleOfferLink::class); 
    }

    //Переименовываем стандартные названия
    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';
}
