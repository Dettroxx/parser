<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\OfferPrepareController;

class OffersSitefy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offers:sitefy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Делает привязки новых офферов к сайтам';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new OfferPrepareController();
        $controller->makeSiteLinks();
    }
}
