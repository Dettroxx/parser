<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ExportController;

class VideoCheck extends Command
{
    protected $signature = 'video:check';
    protected $description = 'Производит проверку';
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $controller = new ExportController();
        $controller->checkVideos();
    }
}
