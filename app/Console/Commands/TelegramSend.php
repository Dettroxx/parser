<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ExportController;

class TelegramSend extends Command
{
    protected $signature = 'telegram:send';
    protected $description = 'Отправляет статистику в телеграм';
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $controller = new ExportController();
        $controller->sendTelegram();
    }
}
