<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\OfferPrepareController;

class OffersDeleteExplicit extends Command 
{
    protected $signature = 'offers:delete_explicit';
    protected $description = 'Удаляет распарсенные принудительные офферы';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $controller = new OfferPrepareController();
        $controller->explicitOffersHandle();
    }
}
