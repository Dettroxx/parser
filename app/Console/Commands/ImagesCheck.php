<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ExportController;

class ImagesCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:images_check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверяет правильность изображенией (чтобы ссылались на текущий)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new ExportController();
        $controller->checkImages();
    }
}
