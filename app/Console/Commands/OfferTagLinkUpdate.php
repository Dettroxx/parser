<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\OfferPrepareController;

class OfferTagLinkUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offers:tags_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновляет ссылки в самих офферах';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new OfferPrepareController();
        $controller->prepareOffersUrls();
    }
}
