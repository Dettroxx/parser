<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    { 
        $schedule->command('import:import_offers')->cron('*/2 * * * *');
        $schedule->command('export:mark_invalid')->cron('*/2 * * * *');
        $schedule->command('import:articles')->cron('*/3 * * * *'); //Импорт новых статей
        $schedule->command('import:fetch_offers')->cron('*/3 * * * *'); //Вытягивает ссылки из статей и производит анализ количества символов
        $schedule->command('articles:handle_invalid')->cron('*/3 * * * *'); //Проверка невалидности статей
        $schedule->command('offers:sitefy')->cron('*/3 * * * *'); //Привязка новых офферов к сайтам
        $schedule->command('offers:delete_explicit')->cron('*/3 * * * *'); //Удаление необходимых для распарсинга офферов из списка распарсенных 
        $schedule->command('offers:tags_update')->cron('*/3 * * * *'); //Обновление правильных url в ссылках офферов
        $schedule->command('offers:update_hrefs')->cron('*/3 * * * *'); //Обновление правильных url в тексте самих статей
        $schedule->command('articles:images_check')->cron('*/3 * * * *'); //Проверка что изображения в статьях верные
        $schedule->command('tree:make')->cron('*/3 * * * *'); //Производит обновление дерева
        $schedule->command('video:check')->cron('*/1 * * * *'); //Проверка видеозаписей
        $schedule->command('telegram:send')->cron('0 */6 * * *'); //Отправка статистики по телеграму
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
