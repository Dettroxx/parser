<?php
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Encore\Admin\Traits\ModelTree;

trait TreeCategoryTrait
{
    use ModelTree {
        ModelTree::saveWithHistory as parentSaveWithHistory;
    }

    public function saveWithHistory()
    {
        $this->parentSaveWithHistory();
        //your implementation
    }
}