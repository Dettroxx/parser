<?php

namespace App\Admin\Controllers;

use App\AmazonCategory;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Site;
use DB;

class AmazonCategoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Список категорий Amazon');
            $content->description('для парсинга офферов');
            $content->body($this->grid());

            //Опции для сайта
            $this->sitesOptions = $this->sitesOptions();
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Категория Amazon');
            $content->description('детальная информация');

            $content->body(Admin::show(AmazonCategory::findOrFail($id), function (Show $show) {
                $show->id();
                $show->created_at();
                $show->updated_at();
                $show->site_id();
                $show->name();
                $show->url();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редактировать категорию Amazon');
            $content->description('для парсинга офферов');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Добавить категорию Amazon');
            $content->description('для парсинга офферов');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(AmazonCategory::class, function (Grid $grid) {
            global $self;
            $self = $this;
            $grid->disableRowSelector();
            $grid->disableExport();
            $grid->column('site_id', 'Сайт')->display(function ($site_id) {
                global $self;
                return $self->sitesOptions[$site_id];
            });
            $grid->column('name', 'Наименование');
            $grid->column('url', 'Ссылка');

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->disableIdFilter();

                //Фильтрация по сайтам
                $sites = $this->sitesOptions;
                foreach ($sites as $key => $value) {
                    $filter->scope('site_id'.$key, 'Сайт '.$value)->where('site_id', "=", $key);
                }

                //Фильтр по названию
                $filter->like('name', 'Наименование');
            });

            //Деактивируем ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableView();
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(AmazonCategory::class, function (Form $form) {
            $form->select('site_id', '*Сайт')->rules('required')->options($this->sitesOptions());
            $form->text('name', 'Наименование');
            $form->textarea('url', '*Ссылка')->rules('required')->rows(5);

            //Настройки
            $form->disableReset();
        });
    }

    protected $sitesOptions = [];
    protected function sitesOptions() {
        $result = (array )DB::table("sites")
        ->select("id", "key")
        ->orderBy("id")
        ->get()
        ->pluck("key","id")
        ->toArray();
        return $result;
    }
}
