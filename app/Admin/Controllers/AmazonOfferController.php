<?php

namespace App\Admin\Controllers;

use App\AmazonOffer;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Site;
use DB;

class AmazonOfferController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Одиночные офферы Amazon');
            $content->description('Для парсинга');
            $content->body($this->grid());

            //Опции для сайта
            $this->sitesOptions = $this->sitesOptions();
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(AmazonOffer::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить одиночный оффер Amazon');
            $content->description('для парсинга');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(AmazonOffer::class, function (Grid $grid) {
            //Деактивация ненужных элементов
            $grid->disableRowSelector();
            $grid->disableExport();

            global $self;
            $self = $this;
            $grid->column('site_id', 'Сайт')->display(function ($site_id) {
                global $self;
                return $self->sitesOptions[$site_id];
            });
            $grid->column('code', 'Код');
            $grid->column('url', 'Ссылка');

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->disableIdFilter();

                //Фильтрация по сайтам
                $sites = $this->sitesOptions;
                foreach ($sites as $key => $value) {
                    $filter->scope('site_id'.$key, 'Сайт '.$value)->where('site_id', "=", $key);
                }

                //Фильтр по названию
                $filter->like('code', 'Код оффера');
            });

            //Деактивируем ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableView();
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(AmazonOffer::class, function (Form $form) {
            $form->disableReset();
            $form->select('site_id', '*Сайт')->rules('required')->options($this->sitesOptions());
            $form->textarea('url', '*Ссылка')->rules('required')->rows(5);
        });
    }

    protected $sitesOptions = [];
    protected function sitesOptions() {
        $result = (array )DB::table("sites")
        ->select("id", "key")
        ->orderBy("id")
        ->get()
        ->pluck("key","id")
        ->toArray();
        return $result;
    }
}
