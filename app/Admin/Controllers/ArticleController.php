<?php

namespace App\Admin\Controllers;

use App\Article;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;
use View;
use App\ArticleOfferLink;
use App\Site;
use DiDom\Document;
use DiDom\Element;
use DiDom\Query;

class ArticleController extends Controller
{
    use ModelForm;


    public function index()
    {
        //Инициализация вспомогательных данных
        $this->sitesOptions = $this->sitesOptions();
        $this->postStatuses = $this->postStatuses();
        $this->innerStatuses = $this->innerStatuses();

        return Admin::content(function (Content $content) {
            $content->header('Список постов');
            $content->description('с сайтов');
            $content->body($this->grid()); 
        });
    }


    public function getEditLink($id) {
        $post = Article::where("id", "=", $id)->first();
        $site_id = $post->site_id;
        $post_id = $post->post_id;

        //Получаем ссылку на сайт
        $site = Site::where("id", "=", $site_id)->first();
        if (!$site) return false;

        $link = $site->fulllink;
        return $link.'/post.php?post='.$post_id.'&action=edit';
    }


    //Функция для определенных преобразований перед выводом статьи
    public function prepareContentShow($id, $content) { 
        if (empty($content)) return $content;
        $article = Article::where("id", "=", $id)->first();
        $site_id = $article->site_id;
        $site = Site::where("id", "=", $site_id)->first();
        if (!$site) return $content;
        $base_url = $site->base_url;
        if (empty($base_url)) return $content;

        $document = new Document();
        $document->loadHtml($content);
        $items = $document->find("//img[not(starts-with(@src, 'http'))]", Query::TYPE_XPATH);

        foreach ($items as $item) {
            $src = $item->attr("src");
            $item->attr("src", $base_url."/".$src);
        }

        $result_content = (string) $document;
        return $result_content;
    }

    public function show($id)
    {
        global $self;
        $self = $this;

        return Admin::content(function (Content $content) use ($id) {
            $content->header('Детальный просмотр поста');
            $content->body(Admin::show(Article::findOrFail($id), function (Show $show) {
                //Деактивируем ненужные кнопки
                $show->panel()
                ->tools(function ($tools) { 
                    $tools->disableEdit();
                    $tools->disableDelete();
                });;

                //Отображение полей
                $show->someother('Редактировать в WP')->as(function ($content) {
                    global $self;
                    $id = $this->id;
                    $fullink = $self->getEditLink($this->id);
                    return '<a href="'.$fullink.'" target="_BLANK">'.$fullink.'</a>';
                });

                $show->post_id('ID поста (WP)');
                $show->post_title('Заголовок');
                $show->post_status('Статус (WP)');
                $show->inner_status('Статус (Backend)')->as(function ($content) {
                    global $self;
                    $statuses = $self->innerStatuses();
                    return $statuses[$content];
                });
                $show->post_modified('Дата изменения');
                $show->divider();
                $show->char_count('Количество символов');
                $show->word_count('Количество слов');
                $show->divider();
                $show->images_invalid('Есть сторонние изображения?')->as(function ($value) {
                    if ($value) {
                        return "Да";
                    } else {
                        return "Нет";
                    }
                });
                $show->videos_invalid('Есть нерабочие видеозаписи?')->as(function ($value) {
                    if ($value) {
                        return "Да";
                    } else {
                        return "Нет";
                    }
                });
                $show->videos_invalid_date('(Дата проверки статьи на валидность видео, если видео в статье нет - не проверяется)');
                $show->divider();
                $show->id('Связанные офферы')->as(function ($article_id) {
                    global $self;
                    $offers = $self->ArticleLinkedOffers($article_id);
                    $view = View::make('ArticleLinkedOffers', ['items' => $offers]); 
                    $contents = (string) $view;
                    return $contents; 
                });
                $show->divider();

                //Отображение контента
                $show->post_content('Контент')->as(function ($content) {
                    global $self;
                    $id = $this->id;
                    $content_result = $self->prepareContentShow($id, $content);
                    return '<div class="backend-article-content">'.$content_result.'</div>';
                });
            }));
        });
    }


    //Подсчет количества символов и слов
    protected function charCount($id = false) {
        $base = DB::table("articles as Article")
        ->select(DB::raw("SUM(Article.char_count) as char_count"), DB::raw("SUM(Article.word_count) as word_count"));
        
        //Для проверки привязанности к сайту
        if ($id) {
            $base = $base->where("site_id", "=", $id);
        }

        $result = $base
        ->get()
        ->toArray();

        return $result[0];
    }


    protected function grid()
    {
        global $self;
        $self = $this;

        return Admin::grid(Article::class, function (Grid $grid) {
            //Фильтрация модели
            $grid->model()
            ->orderBy('post_modified', 'desc'); 

            $grid->disableRowSelector();
            $grid->disableExport();
            $grid->disableCreateButton();

            //Поле сайта
            global $self;
            $self = $this;
            $grid->column('site_id', 'Сайт')->display(function ($site_id) {
                global $self;
                return $self->sitesOptions[$site_id];
            });
            
            $grid->column('post_id', 'ID поста (WP)')->sortable(); 
            $grid->column('post_title', 'Заголовок'); 
            $grid->column('post_status', 'Статус (WP)')->sortable(); 
            $grid->column('inner_status', 'Статус (Backend)')->display(function ($inner_status) {
                global $self;
                if (isset($self->innerStatuses[$inner_status])) {
                    return $self->innerStatuses[$inner_status];
                } else {
                    return $inner_status;
                }
            })->sortable(); 
            $grid->column('char_count', 'Символов')->sortable();
            $grid->column('word_count', 'Слов')->sortable();
            $grid->column('post_modified', 'Изменен')->sortable();
            $grid->column('post_modified', 'Изменен')->sortable();

            //Проверка
            if (isset($_REQUEST['_scope_'])) {
                $id = $_REQUEST['_scope_'];
                $charCount = $this->charCount($id);
            } else {
                $charCount = $this->charCount();
            }
            
            //Выводим обозначение
            $this->char_count = $charCount->char_count;
            $this->word_count = $charCount->word_count;
            $grid->tools(function ($tools) {
                global $self;

                $tools->append('<div class="btn-group pull-right" style="margin-right: 10px; margin-top: 5px">
                    символов: <b>'.$self->char_count.'</b> | слов: <b>'.$self->word_count.'</b>
                </div>');
            });

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->disableIdFilter();
                $sites = $this->sitesOptions;
                foreach ($sites as $key => $value) {
                    $filter->scope($key, 'Сайт '.$value)->where('site_id', "=", $key);
                }
                
                
                $filter->equal('post_id', 'ID поста (WP)');
                $filter->like('post_title','Заголовок');
                $filter->in('post_status', 'Статус (WP)')->select($this->postStatuses);
                $filter->equal('inner_status', 'Статус (Backend)')->select($this->innerStatuses);
                $filter->equal('images_invalid', 'Есть сторонние изображения?')->select([
                    0 => 'Нет',
                    1 => 'Да'
                ]);
                $filter->equal('videos_invalid', 'Есть нерабочие видеозаписи?')->select([
                    0 => 'Нет', 
                    1 => 'Да'
                ]);
                $filter->gt('char_count', 'Количество символов')->integer();
                $filter->between('post_modified', 'Дата изменения')->date();
            });

            //Деактивируем ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableDelete();
                $actions->disableEdit();
            });
        });
    }


    protected function form()
    {
        return Admin::form(Article::class, function (Form $form) {
            $form->disableReset();
            $form->text('post_id', 'ID поста (WP)');
        });
    }


    //Опции по связанным сайтам
    protected $sitesOptions = [];
    protected function sitesOptions() {
        $result = (array )DB::table("sites")
        ->select("id", "key")
        ->orderBy("id")
        ->get()
        ->pluck("key","id")
        ->toArray();
        return $result;
    }


    //Статусы поста
    protected $postStatuses = [];
    protected function postStatuses() {
        $result = (array) DB::table("articles")
        ->select("post_status", "post_status")
        ->orderBy("post_status", "asc")
        ->groupBy("post_status")
        ->get()
        ->pluck("post_status", "post_status")
        ->toArray();
        return $result; 
    }


    //Статусы проверок
    protected $innerStatuses = [];
    public function innerStatuses() {
        return config("custom.inner_statuses");
    }


    //Список связанных офферов с текущим постом
    public function ArticleLinkedOffers($article_id) {
        $offers = (array) ArticleOfferLink::where('article_id', '=', $article_id)->with('offer')->get()->toArray();
        return $offers;
    }
}
