<?php

namespace App\Admin\Controllers;

use App\OfferCategory;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\CustomTree;

class CategoriesController extends Controller
{
    use ModelForm;

    public function sometest() {
        $data = OfferCategory::with('linksCount')->get();

        echo "<pre>";
        print_r($data->toArray());
        echo "</pre>";
    }

    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Категории Amazon');
            $content->body(OfferCategory::tree(function ($tree) {
                $tree->query(function ($model) {
                    return $model->orderBy("text", "asc")->with('linksCount');
                });
                
                $tree->disableCreate();
                $tree->disableSave();

                //Модификация отображения ветки
                $tree->branch(function ($branch) {
                    $link = route('offers_cat', [
                        'category' => $branch['id']
                    ], false);

                    $count = 0;
                    if (isset($branch['links_count'])) {
                        if (count($branch['links_count']) > 0) {
                            $lk = $branch['links_count'][0];
                            $count = $lk['count'];
                        }
                    }
                    return '<a class="tree-collapsex" href="'.$link.'" target="_BLANK">'.$branch['text'].'</a> ('.$count.')';
                });
            }));
        });
    }
}
