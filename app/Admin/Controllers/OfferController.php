<?php

namespace App\Admin\Controllers;

use App\Offer;
use App\OfferCategory;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;
use View;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    use ModelForm;

    protected $current_category = 0;
    protected $current_category_data = null;
    protected $text_data = [];
    protected $id_data = [];
    protected $rec_data = [];

    //Функция, которая обрабатывает чистые данные, полученные в ходе рекурсии
    protected function prepareRawFields() {
        $id = $this->id_data;
        $text = $this->text_data;

        $result = [];
        $count = count($id);
        for ($i = 0; $i < $count; $i++) {
            $result[] = [
                'id' => $id[$i],
                'text' => $text[$i]
            ];
        }

        $this->rec_data = array_reverse($result);
    }

    //Получает данные о текущей категории
    protected function getCurrentCategoryData($id) {
        return OfferCategory::where("id", "=", $id)->with('allParentData')->first()->toArray();
    }

    //Предварительная обработка данных
    protected function prepareCurrentCategory() {
        $data = $this->current_category_data;
        array_walk_recursive($data, array(__CLASS__,'prepare_recursive'));
        $this->prepareRawFields();
    }

    //Рекурсивная обработка
    public function prepare_recursive($item, $key) {
        if ($key == 'text') { 
            $this->text_data[] = $item;
        } else if ($key == 'id') {
            $this->id_data[] = $item;
        }
    }

    public function index($category = 0)
    {
        if (!empty($category)) {
            $this->current_category = $category;
            $this->current_category_data = $this->getCurrentCategoryData($category);
            $this->prepareCurrentCategory();

            
        }

        $this->offerStatuses = $this->offerStatuses();
        return Admin::content(function (Content $content) {
            
            //Формируем описательное обозначение
            if ($this->current_category) {
                $index = count($this->rec_data)-1;
                $content->header($this->rec_data[$index]['text']);
            } else {
                $content->header('Офферы Amazon');
            }

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Offer::findOrFail($id), function (Show $show) {

                $show->id();
                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Edit');
            $content->description('description');
            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Create');
            $content->description('description');
            $content->body($this->form());
        });
    }

    //Привязка офферов к сайту
    public function OffersSiteFilter() {
        $result = (array) DB::table('site_offer_links as Link')
        ->join("sites as Site", "Link.site_id", "=", "Site.id")
        ->orderBy("Site.id", "asc")
        ->groupBy("Link.site_id")
        ->pluck("Site.key", "Site.id")
        ->toArray();

        return $result;
    }


    protected function grid()
    {
        global $self;
        $self = $this;

        return Admin::grid(Offer::class, function (Grid $grid) {
            //Для привязки к категории
            if ($this->current_category) {
                $grid->model()->with('category_links')->whereHas('category_links', function ($query) {
                    $query->where('category_id', '=', $this->current_category);
                });
            } else {
                $grid->model()
                ->orderBy('valid', 'desc')
                ->orderBy('updated_at', 'desc'); 
            }
            

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->disableIdFilter();
                $filter->where(function ($query) {
                    $query->whereHas('site_link', function ($query) {
                        $query->where('site_id', '=', $this->input);
                    });
                }, 'Сайт')->select($this->OffersSiteFilter()); 

                $filter->equal('code', 'ASIN');
                $filter->like('title','Заголовок');
                $filter->equal('valid', 'Валиден')->select(['1' => 'Да', '0' => 'Нет']);
                $filter->in('status', 'Статус')->multipleSelect($this->offerStatuses);
                $filter->between('updated_at', 'Дата обновления')->date();
                $filter->equal('status_code', 'Статус в бекенде')->select([
                    '1' => '1. Спарсен с категории (новый)',
                    '3' => '2. Привязан к сайту'
                ]);
            });

            //Только для случая когда это вывод по категории
            if ($this->current_category) {
                $grid->tools(function ($tools) {
                    global $self;
                    $items = $this->rec_data;
                    $view = View::make('CategoryBreads', ['items' => $items]); 
                    $contents = (string) $view;
                    $tools->append($contents);
                });
            }
            

            //Настройка
            $grid->disableRowSelector();
            $grid->disableExport();
            $grid->disableCreateButton();
            $grid->disableActions(); 

            //Поля
            $grid->column('code', 'ASIN');
            $grid->column('images', 'Изображение')->display(function ($images) {
                $view = View::make('OfferAvatar', ['items' => $images]); 
                $contents = (string) $view;
                return $contents; 
            });
            $grid->column('title', 'Наименование')->display(function ($title) {
                $title = str_limit($title, $limit = 100, $end = '...');
                if (empty($title)) {
                    $title = str_limit($this->url, $limit = 100, $end = '...');
                }
                return '<a href="'.$this->url.'" target="_BLANK">'.$title.'</a>';
            });
            $grid->column('status', 'Статус')->display(function ($status) {
                if ($this->valid) {
                    $color = 'green';
                } else {
                    $color = 'red';
                }
                return '<span style="color: '.$color.';"><b>'.$status.'</b>';
            });
            $grid->column('valid', 'Валиден');
            $grid->column('updated_at', 'Дата обновления');
        });
    }

    protected function form()
    {
        return Admin::form(Offer::class, function (Form $form) {

            $form->display('code', 'ASIN'); 
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    //Статусы поста
    protected $offerStatuses = [];
    protected function offerStatuses() {
        $result = (array) DB::table("offers")
        ->select("status", "status")
        ->orderBy("status", "asc")
        ->groupBy("status")
        ->get()
        ->pluck("status", "status")
        ->toArray();
        return $result;
    }
}
