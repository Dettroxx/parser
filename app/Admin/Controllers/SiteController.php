<?php

namespace App\Admin\Controllers;

use App\Site;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use View;

class SiteController extends Controller
{
    use ModelForm;
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Список сайтов');
            $content->body($this->grid());
        });
    }

    //Функция для генерации ссылки на получении данных 
    public function createLinkExportCSV($site_id, $key) {
        return '<button onclick="javascript: window.location.href = \'/export/exportCSV_Offers/'.$site_id.'\'" class="btn btn-block btn-success hreffed" >Скачать</button>';
    }

    public function show($id)
    {
        global $self;
        $self = $this;

        //Для кастомных скриптов
        $view = View::make('show_sites_scripts');
        $contents = $view->render();

        return Admin::content(function (Content $content) use ($id) {
            $content->header('Список функций для сайта');
            $content->body(Admin::show(Site::findOrFail($id), function (Show $show) {
                $show->panel()
                ->tools(function ($tools) {
                    $tools->disableEdit();
                    $tools->disableDelete();
                });;

                $show->offer_list('Список офферов (Breadcrumb)')->as(function ($none) {
                    global $self;
                    $link = $self->createLinkExportCSV($this->id, $this->key);
                    return $link;
                });
            }));
        }).$contents;
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редактировать сайт');
            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Добавить сайт');
            $content->body($this->form());
        });
    }

    protected function grid()
    {
        return Admin::grid(Site::class, function (Grid $grid) {
            $grid->column('name', 'Наименование');
            $grid->column('url', 'URL экспорта');
            $grid->column('key', 'Уникальный псевдоним');
            $grid->column('active', 'Активен');

            //Настройка функционала
            $grid->disablePagination();
            $grid->disableExport();
            $grid->disableRowSelector();

             //Деактивируем ненужные элементы
             $grid->actions(function ($actions) {
              
            });
        });
    }

    protected function form()
    {
        return Admin::form(Site::class, function (Form $form) {
            $form->text('name', 'Наименование');
            $form->text('url', 'URL экспорта')->rules('required');
            $form->text('fulllink', '*Полная ссылка на админку WP')->rules('required');
            $form->text('key', '*Уникальный псевдоним')->rules('required');
            $form->text('offer_tag', 'Тег offer'); 
            $form->switch('active', 'Активен')->states($this->yesno);
            $form->text('base_url', '*Полный url сайта (включая https и т.д.)')->rules('required');
            $form->divider();

            $form->embeds('start_params', 'Дополнительные параметры' , function ($form) {
                $form->text('static_query', 'Статичные параметры');
            });

            //Настройка
            $form->disableReset();
        });
    }

    protected $yesno = [
        'Да'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
        'Нет' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'], 
    ];
}
