<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', function() {
        return redirect('db/articles');
    });
    $router->resource('config/sites', SiteController::class);
    $router->resource('config/categories', AmazonCategoryController::class);
    $router->resource('config/offers', AmazonOfferController::class);
    $router->resource('db/articles', ArticleController::class); 
    $router->resource('db/offers', OfferController::class); 
    $router->get('db/offers-categoried/{category}', 'OfferController@index')->name("offers_cat");
    $router->resource('db/categories', CategoriesController::class); 
});
