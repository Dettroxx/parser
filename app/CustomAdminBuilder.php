<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Encore\Admin\Traits\AdminBuilder;
use App\CustomTree;

trait CustomAdminBuilder
{
    public static function tree(\Closure $callback = null)
    {
        return new CustomTree(new static(), $callback);
    }
}