<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use App\AmazonOffer;
use App\SiteOfferLink;
use DB;
use League\Url\Url;
use DiDom\Document;
use DiDom\Element;
use DiDom\Query;
use App\Article;
use App\OfferCategory;
use App\OfferCategoryLink;
use League\Uri;

class OfferPrepareController extends Controller
{
    //Делает связи для вновь импортированных офферов
    public function makeSiteLinks() {
        $items = $this->fetchRawOffers();
        $this->insertLinks($items);
    }


    protected function fetchRawOffers() {
        return
        DB::table("offers")
        ->select("id", "site_id")
        ->whereNotNull("site_id")
        ->get()
        ->toArray();
    }


    protected function insertLinks($items) {
        $result = [];
        $unset_ids = [];

        foreach ($items as $item) {
            $unset_ids[] = $item->id;
            $cast_site_id = (int) $item->site_id;
            if ($cast_site_id <= 0) continue;

            $result[] = [
                "site_id" => $cast_site_id,
                "offer_id" => $item->id
            ];
        }
        
        SiteOfferLink::insert($result); //Вставка
        DB::table("offers") //Очистка
        ->whereIn("id", $unset_ids)
        ->update([
            'site_id' => '',
            'status_code' => 3
        ]);
    }


    //Для удаления уже спарсенных офферов из принудительного парсинга офферов и привязки их нужному сайту
    public function explicitOffersHandle()  {
        $explicit_parsed = $this->getExplicitOffersParsed();
        $this->PrepareExplicitLinks($explicit_parsed); //Заносим связи
        $this->PrepareDeleteExplicit($explicit_parsed); //Удаляем офферы из списка принудительных, которые уже существуют
    }


    protected function getExplicitOffersParsed() {
        return (array)
        DB::table("amazon_offers as AmazonOffer")
        ->select("Offer.id as offer_id", "AmazonOffer.site_id as site_id", "AmazonOffer.id as id")
        ->join("offers as Offer", "AmazonOffer.code", "=", "Offer.code")
        ->get()
        ->toArray();
    }


    protected function PrepareExplicitLinks($items) {
        $result = [];
        foreach ($items as $item) {
            $result[] = [
                "offer_id" => $item->offer_id,
                "site_id" => $item->site_id
            ];
        }

        SiteOfferLink::insert($result);
    }


    protected function PrepareDeleteExplicit($items) {
        $ids = [];
        foreach ($items as $item) {
            $ids[] = $item->id;
        }

        AmazonOffer::whereIn("id", $ids)->delete();
    }


    //Для пометки невалидных статей на основе невалидных офферов
    public function markArticlesInvalid() {
        //Обновляем невалидные статьи
        $ids = $this->fetchInvalidArticles();
        $this->updateInvalidArticles($ids);

        //Остальные статьи прогоняем еще раз и делаем валидными
        $this->updateValidArticles($ids);
    }


    //Получает id статей, которые стали невалидными
    protected function fetchInvalidArticles() {
        return (array) DB::table("articles as Article")
        ->join("article_offer_links as Link", "Article.id", "=", "Link.article_id")
        ->join("offers as Offer", "Link.offer_id", "=", "Offer.id")
        ->where("Offer.valid", "!=", "1")
        ->pluck("Article.id")
        ->toArray();
    }


    //Помечает невалидные статьи
    protected function updateInvalidArticles($ids) {
        DB::table("articles")
        ->whereIn("id", $ids)
        ->update([
            "inner_status" => 50,
            'post_status' => 'pending'
        ]);
    }


    //Помечает валидные статьи
    protected function updateValidArticles($ids) {
        DB::table("articles")
        ->whereNotIn("id", $ids)
        ->where("inner_status", "=", 50)
        ->update([
            "inner_status" => 10,
            'post_status' => 'draft'
        ]);
    }


    //Для обработки URL офферов
    public function prepareOffersUrls() {
        $urls = $this->getOffersUrls();
        $this->parseUrls($urls);
    }


    //Возвращает сортированный массив данных
    protected function getSortedParams($tag, $params) {
        $result = [
            'tag' => $tag
        ];

        foreach ($params as $key => $value) {
            if ($key == 'tag') continue;
            $result[$key] = $value;
        }

        return $result;
    }


    //Парсинг url ссылок
    protected function parseUrls($urls) {
        $result = array();

        foreach ($urls as $url) {
            //Производим парсинг ссылки
            $parsed = Uri\parse($url->url);
            $params = parse_str($parsed['query'], $out_params);
            
            //Производим сортировку параметров
            $sorted_params = $this->getSortedParams($url->tag, $out_params);
            $parsed['query'] = http_build_query($sorted_params);  

            //Производим построение нового url
            $new_url = Uri\build($parsed);

            $result[$url->id] = (string) $new_url;
        }

        //Обновление
        foreach ($result as $id => $url) {
            DB::table("offers")
            ->where("id", "=", $id)
            ->update([
                "uniq_link" => $url
            ]);
        }
    }


    //Получаем список данных
    protected function getOffersUrls() {
        return (array)
        DB::table("offers as Offer")
        ->select("Offer.id as id", "Offer.url as url", "Site.offer_tag as tag")
        ->join("site_offer_links as Link", "Offer.id", "=", "Link.offer_id")
        ->join("sites as Site", "Link.site_id", "=", "Site.id")
        ->where("Offer.uniq_link", "=", "")
        ->orWhereNull("Offer.uniq_link")
        ->limit(300)
        ->groupBy("Offer.id")
        ->get()
        ->toArray();
    }


    //Производит замену ссылок в статьях на аналогичные, но верно измененные 
    public function replaceLinks() {
        $ids = $this->replaceLinksFetch();
        $articles = $this->getArticles($ids);

        //Производим замену ссылок в офферах
        foreach ($articles as $article) {
            $this->prepareUniqLinksArticle($article);
        }
    }

    
    protected function replaceLinksFetch() {
        $limit = 500;
        return (array)
        DB::table("articles as Article")
        ->join("article_offer_links as Link", "Article.id", "=", "Link.article_id")
        ->join("offers as Offer", "Link.offer_id", "=", "Offer.id")
        ->whereNotNull("Offer.uniq_link")
        ->orderBy("Article.post_modified", "ASC")
        ->groupBy("Article.id")
        ->limit($limit)
        ->pluck("Article.id")
        ->toArray();
    }


    //Возвращает список статей
    protected function getArticles($ids) {
        return Article::whereIn("id", $ids)->get();
    }


    //Производит обработку одной статьи
    protected function prepareUniqLinksArticle($article) {
        $linked_offers = $this->getLinkedOffers($article->id);
        if (empty($linked_offers)) {
            $aticle->touch();
            return;
        }

        //Производим замену ссылок статей
        $content = $article->post_content;
        $document = new Document();
        $document->loadHtml($content);

        //Выборка ссылок, соответствующих офферам
        $raw_contains = array();
        foreach ($linked_offers as $key => $value) {
            $raw_contains[] = 'contains(@href, "'.$key.'")';
        }
        $contains = implode(' or ', $raw_contains);
        $raw_links = $document->find('//a[contains(@href, "amazon") and ('.$contains.')]', Query::TYPE_XPATH);

        //Обработка ссылок
        foreach ($raw_links as $link) {
            $href = $link->attr("href");
            $asin = $this->fetchASIN($href);
            if (!$asin) continue;

            //Заменяем ссылку
            if (!isset($linked_offers[$asin])) continue;
            $new_href = $linked_offers[$asin];
            if (empty($new_href)) continue;

            $link->attr("href", $new_href);
        }

        //Получаем результирующее значение 
        $result = (string )$document;
        $article->post_content = $result;
        $article->save();
        $article->touch();
    }


    //Выводит список привязанных офферов
    protected function getLinkedOffers($id) {
        $codes = DB::table("article_offer_links as Link")
        ->where("Link.article_id", $id)
        ->join("offers as Offer", "Link.offer_id", "=", "Offer.id")
        ->whereNotNull("Offer.uniq_link")
        ->pluck("Offer.uniq_link", "Offer.code")
        ->toArray();

        return $codes;
    }


    //Выборка кода ASIN из текста
    protected function fetchASIN($url) {
        $pattern = '/\/([A-Z0-9]{10}?)\//';
        $match = preg_match($pattern, $url, $matches);
        if ($match) {
            $code = $matches[1];
            return $code;
        } else {
            $pattern = '/%2F([A-Z0-9]{10}?)%2F/';
            $match = preg_match($pattern, $url, $matches);
            if ($match) {
                $code = $matches[1];
                return $code;
            } else {
                $pattern = '/\/([A-Z0-9]{10}?)/';
                $match = preg_match($pattern, $url, $matches); 
                if ($match) {
                    $code = $matches[1];
                    return $code;
                } else {
                    $pattern = '/\/([A-Z0-9]{10}?)\?/';
                    $match = preg_match($pattern, $url, $matches); 
                    if ($match) {
                        $code = $matches[1];
                        return $code;
                    } else {
                        return false;
                    }
                }
            }
        }
    }


    //Генерация категорий Amazon
    public function generateTree() {
        $ids = $this->fetchOffersGenerate();
        $this->generateCategories($ids);
    }


    //Выдает список id офферов соответствующих
    protected function fetchOffersGenerate() {
        $limit = 200;
        return (array)
        DB::table("offers as Offer")
        ->whereNotNull("Offer.path")
        ->orderBy("Offer.updated_at", "ASC")
        ->groupBy("Offer.id")
        ->limit($limit)
        ->pluck("Offer.id")
        ->toArray();
    }


    //Создает дерево категорий
    private function generateCategories($ids) {
        foreach ($ids as $id) {
            $element = Offer::where("id", "=", $id)->first();
            if (empty($element)) {
                $element->touch();
                continue;
            }
            $this->prepareCategories($element);
            $element->touch();
        }
    }


    //Обрабатывает один элемент категорий
    protected function prepareCategories($item) {
        $path = $item->path;
        if (empty(trim($path))) return;
        $path = explode("|", $path);
        $this->createCategoriesIfNotExist($path);
        $this->createCategoriesLinksIfNotExist($item, $path);
    }


    //Создает список категорий
    private function createCategoriesIfNotExist($path) {
        foreach ($path as $level => $name) {
            $this->createCategoryOne($level, $name, $path);
        }
    }


    //Создает категории
    private function createCategoryOne($level, $name, $path) {
        $category = OfferCategory::where("level", "=", $level)->where("text", "=", $name)->first();
        if ($category) return;

        //Если не корневой элемент
        $parent = null;
        if ($level > 0) {
            $parent_level = $level-1;
            $parent_name = $path[$parent_level];
            $parent_category = OfferCategory::where("level", "=", $parent_level)->where("text", "=", $parent_name)->first();
            if (!empty($parent_category)) {
                $parent = $parent_category->id;
            }
        }

        //Создаем элемент категории
        $obj = new OfferCategory();
        $obj->parent = $parent;
        $obj->level = $level;
        $obj->text = $name;
        $obj->save();
    }


    //Генерирует ссылки
    private function createCategoriesLinksIfNotExist($item, $path) {
        $item_id = $item->id;
        foreach ($path as $level => $name) {
            $category = OfferCategory::where("level", "=", $level)->where("text", "=", $name)->first();
            if (empty($category)) continue;
            $category_id = $category->id;
            $this->createCategoryLinksOne($item_id, $category_id);
        }

        $item->path = null;
        $item->save();
    }


    //Создает одну связь
    private function createCategoryLinksOne($item_id, $category_id) {
        $obj = new OfferCategoryLink();
        $obj->offer_id = $item_id;
        $obj->category_id = $category_id;
        $obj->save();
    }
}
