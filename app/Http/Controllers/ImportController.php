<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use App\Offer;
use App\Site;
use DB;
use App\Article;
use DiDom\Document;
use DiDom\Query;
use App\ArticleOfferLink;
use App\ImportOffer;
use App\OfferCategory;
use App\OfferCategoryLink;
use App\SiteOfferLink;


class ImportController extends Controller
{
    protected $affected = 0;
    protected $site = null;

    protected function prepareAdditionalSiteParams($site) {
        $start_params = $site->start_params;
        if (!$start_params) return array();
        if (!isset($start_params['static_query']) || empty($start_params['static_query'])) return array();
        $query = $start_params['static_query'];
        $query = explode('&', $query);
        $result = array();
        foreach ($query as $item) {
            if (empty(trim($item))) continue;
            $data = explode("=", $item);
            if (!isset($data[0])) continue;
            $key = $data[0];
            if (isset($data[1])) {
                $value = $data[1];
            } else {
                $value = null;
            }

            $result[$key] = $value;
        }

        return $result;
    }

    public function import() {
        $this->affected = 0;

        //Получаем самый "старый сайт"
        $site = Site::where('active', '=', true)
        ->orderBy('updated_at', 'asc')->first();
        if (empty($site)) return;
        $this->site = $site;

        $import_config = $site->import_config;

        //Если пуст задаем базовый конфиг
        if (empty($import_config)) {
            $import_config = [
                'limit' => 1000,
                'offset' => 0
            ];
        } else {
            $import_config = json_decode($import_config, true);
        }

        //Получаем дополнительные параметры
        $addition_params = $this->prepareAdditionalSiteParams($site);
        $import_config = array_merge($addition_params, $import_config);
        
        //Принудительно повышаем limit
        $import_config['limit'] = 1000;

        //Получаем конфигурация для парсинга сайта
        $site_config = $this->getSiteConfig($site);

        //Получаем данные
        $data = $this->get($site->url, $import_config);

        //Производим первичную обработку
        $data = $this->prepare($data, $site_config);

        //Производим занесение в таблицы данных
        $this->prepareNew($data);

        //Производим сохранение нового конфига
        if (count($data) > 0) {
            $import_config['offset'] += $import_config['limit'];
        } else {
            $import_config['offset'] = 0;
        }
        $site->import_config = json_encode($import_config);
        $site->touch(); 
        $site->save();
    }

    //Возвращает конфигурацию парсинга для текущего сайта
    protected function getSiteConfig($site) {
        return [
            "site_id" => $site->id
        ];
    }

    //Получает данные
    protected function get($url, $params = []) {
        $client = new Client();
        try {
            $response = $client->request('GET', $url, [
                'query' => $params,
                'timeout' => 20
            ]); 
        } catch (ConnectException $e) {
            $this->site->touch();
            exit();
        }
        
        return json_decode($response->getBody()->getContents(), true);
    }

    //Производит первичную обработку
    protected function prepare($data, $config) {
        $site_id = $config["site_id"];
        foreach ($data as &$post) {
            $id = $post["ID"];
            $post["post_id"] = $id;
            $post["site_id"] = $site_id;
        }
        return $data;
    }

    //Производит подготовку данных для вставки (новые)
    protected function prepareNew($data) {
        $columns = DB::getSchemaBuilder()->getColumnListing('articles');
        $columns = array_flip($columns);

        //Фикс для даты создания
        if (isset($columns['post_date'])) {
            unset($columns['post_date']);
        }

        //Обработка
        $total = array();

        foreach ($data as $item) {
            $prepared = array_intersect_key($item, $columns);

            //Проверяем существует - ли такой пост
            $exist = Article::where('site_id', '=', $prepared['site_id'])
            ->where('post_id', '=', $prepared['post_id'])
            ->first();

            //Если существует, то можно - ли его обновить
            if ($exist) {
                $md5_import = md5($prepared['post_content']);
                $md5_exist = md5($exist->post_content);

                if ($md5_import == $md5_exist) continue; //Проверка

                //Иначе производим обновление
                $prepared['inner_status'] = 0;
                $exist->fill($prepared);
                $exist->save();

                //Удаляем связанные ссылки офферов
                DB::table('article_offer_links')->where('article_id', '=', $exist->id)->delete();
                $this->affected++;
                continue;
            }

            //Для цикличности парсинга
            if (count($prepared) > 0) {
                $this->affected += count($prepared);
            }
            $total[] = $prepared;
        }

        //Вставка
        Article::insert($total);
    }

    //Функция для парсинга одной статьи
    public function parseOfferlinks() {
        $articles = Article::orderBy('post_modified', 'asc')
        ->where('inner_status', '=', 0)
        ->limit(500)
        ->get();
        
        //Обработка всех по списку
        foreach ($articles as $article) {
            $content = $article->post_content;
            if (empty(trim($content))) { //Если пусто - значит статья без офферов
                $article->inner_status = 60; 
                $article->touch();
                $article->save();
                continue;
            }
            $links = $this->parseOneArticle($content);

            //Генерация связей
            $this->handleNewOffersParseLinks($links);
            $this->handleArticleOfferLinks($article,$links); 

            //Меняем статус, только если были ссылки
            if (count($links) > 0) {   
                $article->inner_status = 10; 
            } else {
                $article->inner_status = 60; 
            }

            //Подсчет количества символов
            $this->countChars($article);

            $article->touch();
            $article->save();
        }
    }

    public function sometest() {
        $text = file_get_contents(storage_path().'/1.txt');
        $word_match = preg_match_all("/[A-z0-9А-я]{2,}/", $text, $out, PREG_SET_ORDER);
        if ($word_match) {
            $word_count = count($out);
        } else {
            $word_count = 0;
        }
    }

    //Производит подсчет символов
    protected function countChars($article) {
        $content = $article->post_content;
        $document = new Document();
        $document->loadHtml($content); 
        $text = $document->text();

        $data = preg_replace('/\s+/S', '', $text); 
        $len = strlen($data);
        $article->char_count = $len;

        //Дополнительно подсчет количества слов
        $word_match = preg_match_all("/[A-z0-9А-я]{2,}/", $text, $out, PREG_SET_ORDER);
        if ($word_match) {
            $word_count = count($out);
        } else {
            $word_count = 0;
        }
        $article->word_count = $word_count;
    }


    //Производит парсинг одной статьи
    protected function parseOneArticle($content) {
        $links = array();

        //Получаем список всех ссылок
        $document = new Document();
        $document->loadHtml($content); 
        $raw_links = $document->find('//a[contains(@href, "amazon.com")]', Query::TYPE_XPATH);

        //Обходим 
        foreach ($raw_links as $link) {
            $url = $link->attr('href');

            //Нахождение точного asin-кода
            $pattern = '/\/([A-Z0-9]{10}?)\//';
            $match = preg_match($pattern, $url, $matches);
            if ($match) {
                $code = $matches[1];
            } else {
                $pattern = '/%2F([A-Z0-9]{10}?)%2F/';
                $match = preg_match($pattern, $url, $matches);
                if ($match) {
                    $code = $matches[1];
                } else {
                    $pattern = '/\/([A-Z0-9]{10}?)/';
                    $match = preg_match($pattern, $url, $matches); 
                    if ($match) {
                        $code = $matches[1];
                    } else {
                        continue;
                    }
                }
            }

            //Формируем массив
            $links[] = array(
                'code' => $code,
                'url' => $url
            );
        }

        return $links;
    }

    //Несуществующие офферы при парсинге в базу
    protected function handleNewOffersParseLinks($links) { 
        //Кеирование массива по полю code
        $keyed_links = array();
        foreach ($links as $item) {
            $keyed_links[$item['code']] = $item;
        }

        //Получение списка кодов
        $codes = $this->CodesFromLinks($links);

        $offer_codes = Offer::whereIn('code', $codes)->pluck('code')->toArray();
        $insert_codes = array_diff($codes, $offer_codes);

        //Отбор элементов
        $insert_links = array();
        foreach ($insert_codes as $item) {
            $insert_links[] = $keyed_links[$item];
        }

        //Вставка элементов
        Offer::insert($insert_links);
    }

    //Список кодов по списку ссылок
    protected function CodesFromLinks($links) {
        $codes = array();
        foreach ($links as $item) {
            if (!in_array($item['code'], $codes)) $codes[] = $item['code'];
        }
        return $codes; 
    }

    //Генерирует связанные ссылки
    protected function handleArticleOfferLinks($article, $links) {
        $codes = $this->CodesFromLinks($links);
        $ids = Offer::whereIn('code', $codes)->pluck('id')->toArray();

        //Удаляем старые ссылки Статья-оффер
        ArticleOfferLink::where('article_id', "=", $article->id)
        ->delete();

        //Удаляем старые ссылки Сайт-оффер
        SiteOfferLink::whereIn('offer_id', $ids)
        ->where("site_id", "=", $article->site_id)
        ->delete();

        //Добавляем
        $insert1 = array();
        $insert2 = array();
        foreach ($ids as $id) {
            $insert1[] = array(
                "article_id" => $article->id,
                "offer_id" => $id
            );

            $insert2[] = array(
                "site_id" => $article->site_id,
                "offer_id" => $id
            );
        }

        //Вставка данных
        ArticleOfferLink::insert($insert1);
        SiteOfferLink::insert($insert2);
    }


    //Производит импорт офферов из таблицы импорта
    public function importOffers() {
        $new_offers_ids = $this->new_offers();
        $exist_offers_ids = $this->exists_offers();

        //Импортируем новые офферы
        $this->insertNewOffers($new_offers_ids);
        $this->updateExistOffers($exist_offers_ids);
    }


    private function updateExistOffers($ids) {
        $max_offer_parse = config("appconfig.max_offer_parse") ?? 50;
        $import = ImportOffer::whereIn("code", $ids)->take($max_offer_parse)->get()->toArray();
        $prepared_import = $this->prepareImport($import);
        $selected_offer_ids = $this->getSelectedOfferIds($prepared_import);

        //Фильтрация похожих (одинаковых значений)
        $filtered_import = $this->afterFilter($prepared_import);

        //Подготовка данных для вставки
        $insert_import = $this->insertImport($filtered_import, true);

        //Производим обновление данных
        $this->updateOffers($insert_import);

        //Производим обновление для "невалидных данных"
        $this->processNotValidUpdate($insert_import);

        //Производим выборку данных с привязкой офферов к сайтам
        $this->prepareExplicit($prepared_import);

        //Производим удаление обновленных постов
        ImportOffer::whereIn("code", $selected_offer_ids)->delete();
    }


    //Производит обновление принудительно созданных офферов
    private function prepareExplicit($items) {
        $site_keys = (array) DB::table('sites')
        ->pluck('key')  
        ->toArray();  

        //Отсеивание
        $explicit = array();
        foreach ($items as $item) {
            $site_key = $item["site_key"];
            if (!$site_key) continue;
            if (!in_array($site_key, $site_keys)) continue;

            $explicit[] = $item;
        }

        //Список кодов
        $codes = array();
        foreach ($explicit as $item) {
            $code = $item["code"];
            if (in_array($code, $codes)) continue;
            $codes[] = $item["code"];
        }

        //Список сайтов
        $sites = array();
        foreach ($explicit as $item) {
            $site = $item["site_key"];
            if (in_array($site, $sites)) continue;
            $sites[] = $item["site_key"];
        }

        //Получение списка офферов по коду
        $code_ids = (array) DB::table("offers")
        ->whereIn("code", $codes)
        ->pluck("id", "code")
        ->toArray();
        
        //Получение списка сайтов по коду
        $site_ids = (array) DB::table("sites")
        ->whereIn("key", $sites)
        ->pluck("id", "key")
        ->toArray();

        //Теперь проходимся по списку и формируем список для вставки
        $update = array();
        foreach ($explicit as $item) {
            $code = $item["code"];
            $site = $item["site_key"];
            if (!isset($code_ids[$code]) || !isset($site_ids[$site])) continue;
            $update[] = array(
                "offer_id" => $code_ids[$code],
                "site_id" => $site_ids[$site]
            );
        }

        //Сортируем только новые
        $new_prepared = array();
        foreach ($update as $item) {
            $count = DB::table("site_offer_links")
            ->where("site_id", "=", $item["site_id"])
            ->where("offer_id", "=", $item["offer_id"])
            ->count();
            if ($count > 0) continue;
            DB::table("site_offer_links")->insert($item);
        }
    }


    //Функция, которая производит обновление невалидных данных
    private function processNotValidUpdate($data) {
        $not_valid = array();
        foreach ($data as $item) {
            $valid = $item["valid"];
            if ($valid) continue;
            $not_valid[] = $item["code"];
        }

        //Производим цикл работы над обновлением элементов
        $offers_ids = DB::table("offers")->whereIn("code", $not_valid)->pluck("id")->toArray();
        $article_ids = DB::table("article_offer_links")->whereIn("offer_id", $offers_ids)->pluck("article_id")->toArray();
        DB::table("articles")->whereIn('id', $article_ids)->update([
            'inner_status' => 50
        ]);
    }


    private function updateOffers($data) {
        foreach ($data as $item) {
            $offer_id = $item["code"];
            $element = Offer::where("code", "=", $offer_id)->first();
            if (empty($element)) continue;

            //Иначе обновляем и производим проверку статусов
            $element->fill($item);
            $element->touch();
            $element->save();
        }
    }


    private function unique_offers() {
        return ImportOffer::select("code")->distinct()->pluck("code")->toArray();
    }

    private function exists_offers() {
        $unique = $this->unique_offers();
        return Offer::select("code")->distinct()->whereIn("code", $unique)->pluck("code")->toArray();
    }

    private function new_offers() {
        $unique = $this->unique_offers();
        $exists = $this->exists_offers();
        return array_diff($unique, $exists);
    }

    
    //Импорт новых офферов
    protected function insertNewOffers($ids) {
        $max_offer_parse = config("appconfig.max_offer_parse") ?? 10;
        $import = ImportOffer::whereIn("code", $ids)->take($max_offer_parse)->get()->toArray();
        $prepared_import = $this->prepareImport($import);
        $selected_offer_ids = $this->getSelectedOfferIds($prepared_import);
        $filtered_import = $this->afterFilter($prepared_import);
        $insert_import = $this->insertImport($filtered_import);
        $insert_import = $this->filterOnlyValid($insert_import);

        //Вставка данных
        Offer::insert($insert_import);

        //Производим генерацию категорий
        $this->generateCategories($insert_import);

        //Удаляем спарсенные элементы
        ImportOffer::whereIn("code", $selected_offer_ids)->delete();
    }


    //Новая категория
    private function generateCategories($insert_import) {
        foreach ($insert_import as $item) {
            $element = Offer::where("code", "=",$item['code'])->first();
            if (empty($element)) continue;
            $this->prepareCategories($element);
        }
    }


    ///Производим обработку категорий
    private function prepareCategories($item) {
        $path = $item->path;
        if (empty($path) || !is_array($path)) return;

        $this->createCategoriesIfNotExist($path);
        $this->createCategoriesLinksIfNotExist($item, $path);
    }

    private function createCategoriesIfNotExist($path) {
        foreach ($path as $level => $name) {
            $this->createCategoryOne($level, $name, $path);
        }
    }


    private function createCategoryOne($level, $name, $path) {
        $category = OfferCategory::where("level", "=", $level)->where("text", "=", $name)->first();
        if ($category) return;

        //Если не корневой элемент
        $parent = null;
        if ($level > 0) {
            $parent_level = $level-1;
            $parent_name = $path[$parent_level];
            $parent_category = OfferCategory::where("level", "=", $parent_level)->where("text", "=", $parent_name)->first();
            if (!empty($parent_category)) {
                $parent = $parent_category->id;
            }
        }

        //Создаем элемент категории
        $obj = new OfferCategory();
        $obj->parent = $parent;
        $obj->level = $level;
        $obj->text = $name;
        $obj->save();
    }


    private function createCategoriesLinksIfNotExist($item, $path) {
        $item_id = $item->id;
        foreach ($path as $level => $name) {
            $category = OfferCategory::where("level", "=", $level)->where("text", "=", $name)->first();
            if (empty($category)) continue;
            $category_id = $category->id;
            $this->createCategoryLinksOne($item_id, $category_id);
        }
    }


    private function createCategoryLinksOne($item_id, $category_id) {
        $obj = new OfferCategoryLink();
        $obj->offer_id = $item_id;
        $obj->category_id = $category_id;
        $obj->save();
    }


    //Подготавливает данные для импорта
    private function insertImport($import, $need_update = false) {
        $result = array();
        foreach ($import as $item) {
            $element = array();
            $element["code"] = $item["code"];

            //Из - за особенностей
            if (!$need_update) {
                $element["path"] = json_encode($item["path"]);
                $element["images"] = json_encode($item["images"]);
                $element["thumbs"] = json_encode($item["thumbs"]);
                $element["sizes"] = json_encode($item["sizes"]);
                $element["colors"] = json_encode($item["colors"]);
            } else {
                $element["path"] = $item["path"];
                $element["images"] = $item["images"];
                $element["thumbs"] = $item["thumbs"];
                $element["sizes"] = $item["sizes"];
                $element["colors"] = $item["colors"];
            }

            $element["title"] = $item["title"];
            $element["stars"] = $item["stars"];


            $element["status"] = $item["status"];
            $element["content"] = $item["content"];
            $element["url"] = $item["url"];
            $element["price_current"] = $item["price"];
            $element["price_old"] = $item["old_price"];
            $element["parsed"] = true;

            //Чекинг на статусы
            $isOfferValid = $this->validateOffer($item);
            $element["valid"] = $isOfferValid;

            //Если это обновление - дополнительно производим такое
            if (!$need_update) {
                $element["created_at"] = date('Y-m-d G:i:s');
            } else {
                $element["updated_at"] = date('Y-m-d G:i:s');
            }

            $result[] = $element;
        }

        return $result;
    }


    private function filterOnlyValid($insert_import) {
        $result = array();
        $no_valid = array();
        foreach ($insert_import as $item) {
            $valid = $item["valid"];
            if (!$valid) {
                $no_valid[] = $item["code"];
            } else {
                $result[] = $item;
            }
        }

        return $result;
    }



    //Функция для проверки валидности статусов
    private function validateOffer($data) {
        if (!empty($data['valid_attr'])) {
            return true;
        } else {
            return false;
        }
    }


    //Список Offer_ID которые мы используем
    private function getSelectedOfferIds($prepared_import) {
        $result = array();
        foreach ($prepared_import as $item) {
            $result[] = $item["code"];
        }
        return $result;
    }


    private function afterFilter($insert_import) {
        $result = array();
        foreach ($insert_import as $item) {
            $code = $item["code"];
            $result[$code] = $item;
        }

        return array_values($result);
    }


    //Начальная обработка данных с таблицы импорта
    private function prepareImport($import) {
        $result = array();
        foreach ($import as &$item) {
            $item["path"] = $this->addOneFilter(explode("|", $item["path"]));
            $item["stars"] = (double)$item['stars'];
            $item["price"] = (double)$item['price'];
            $item["old_price"] = (double)$item['old_price'];
            $item["images"] = $this->addOneFilter(explode("|", $item["images"]));
            $item["thumbs"] = $this->addOneFilter(explode("|", $item["thumbs"]));
            $item["sizes"] = $this->addOneFilter(explode("|", $item["sizes"]));
            $item["colors"] = $this->addOneFilter(explode("|", $item["colors"]));
            $result[] = $item;
        }
        return $result;
    }

    //Функция для дополнительной фильтрации массивных данных
    private function addOneFilter($arr) {
        $result = array();
        foreach ($arr as $item) {
            $item_element = trim($item);
            if (empty($item_element)) continue;
            $result[] = $item_element;
        }

        return $result;
    }
}
