<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Site;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ExcelExportController extends Controller 
{
    public function ExportOffersBreadcrumbs($site_id) {
        $query = $this->createExportOffersQuery($site_id);
        $filename = $this->createExportOffersFileName($site_id);

        //Создаем результирующий поток
        $response = new StreamedResponse(function() use ($query) { 
            $handle = fopen('php://output', 'w');

            /*
            $this->chunkExportOffersQuery(500, $query, function($items) use (&$handle) {
                foreach ($items as $item) {
                    fputcsv($handle, (array) $item);
                }
            });*/

            $elements = (array) $query->limit(100)->get()->toArray();
            foreach ($elements as &$item) {
                $path = $item->path;
                unset($item->path);
                $new_item = (array) $item;
                $new_path = explode("|", $path);
                $total_array = array_merge($new_item, $new_path);
                fputcsv($handle, $total_array, ";");
            }  

            fclose($handle);
        }, 200, [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
        ]);

        return $response;
    }


    //Создает запрос на генерацию списка офферов
    protected function createExportOffersQuery($site_id) {
        $query = DB::table("site_offer_links as SiteOfferLink")
        ->select("Offer.code", "Offer.title", "Offer.uniq_link as link",
            DB::raw("GROUP_CONCAT(DISTINCT OfferCategory.text ORDER BY OfferCategory.level ASC SEPARATOR '|') as path")
        )
        ->join("offers as Offer", "SiteOfferLink.offer_id", "=", "Offer.id")
        ->join("offer_category_links as OfferCategoryLink", "SiteOfferLink.offer_id", "=", "OfferCategoryLink.offer_id")
        ->join("offer_categories as OfferCategory", "OfferCategoryLink.category_id", "=", "OfferCategory.id")
        ->whereNotNull("Offer.uniq_link")
        ->where("valid", "=", true)
        ->where("SiteOfferLink.site_id", "=", $site_id)
        ->groupBy("Offer.id")
        ->orderBy("OfferCategory.text", "asc");

        return $query;
    }


    //Чанкирование вывода
    protected function chunkExportOffersQuery($chunk, $query, $func) {
        $query->chunk($chunk, $func);
    }

    
    //Название файла для сохранения
    protected function createExportOffersFileName($site_id) {
        $site = Site::where("id", "=", $site_id)->first();
        $time = date('d.m.Y_H:i');
        $key = $site->key;
        return $key.'_offers_breadcrumbs_'.$time.'.csv';
    }
}
