<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use DiDom\Document;
use DiDom\Element;
use DiDom\Query;
use DB;
use App\Site;
use View;
use Alaouy\Youtube\Facades\Youtube;


class ExportController extends Controller
{
    public function markInvalidOffers() { 
        $unvalids = $this->unvalids();
        $articles = Article::whereIn("id", $unvalids)
        ->get();

        foreach ($articles as $article) {
            $this->markOneArticle($article);
        }
    }

    public function unvalids() {
        $limit = 100;
        $articles = DB::table("articles as Article")
        ->join('article_offer_links as Link', 'Article.id', '=', 'Link.article_id')
        ->join('offers as Offer', 'Link.offer_id', '=', 'Offer.id')
        ->where('Offer.valid', '=', false)
        ->orderBy("post_modified", "ASC")
        ->groupBy('Article.id')
        ->limit($limit)
        ->pluck('Article.id')
        ->toArray();
        
        return $articles;
    }

    public function markOneArticle($article) {
        //Находим связанный с ним список невалидных офферов
        $article_id = $article->id;
        $codes = DB::table("article_offer_links as Link")
        ->where("Link.article_id", $article_id)
        ->join("offers as Offer", "Link.offer_id", "=", "Offer.id")
        ->where("Offer.valid", "=", false)
        ->pluck("Offer.code")
        ->toArray();

        //Формируем кусок запроса
        if (empty($codes)) {
            $aticle->touch();
            return;
        }

        //Удаляем старые записи о невалидности
        $content = $article->post_content;
        $document = new Document();
        $document->loadHtml($content);
        $unvalidsx = $document->find('//*[@class="not-valid-add"]', Query::TYPE_XPATH);
        foreach ($unvalidsx as $item) {
            $item->remove();
        }


        $raw_contains = array();
        foreach ($codes as $item) {
            $raw_contains[] = 'contains(@href, "'.$item.'")';
        }
        $contains = implode(' or ', $raw_contains);
        $raw_links = $document->find('//a[contains(@href, "amazon") and ('.$contains.')]', Query::TYPE_XPATH);

        //Элемент невалидности
        $not_valid = new Element('span', ' (Not Valid)');
        $not_valid->attr('style', 'color: red; font-weight: bold');
        $not_valid->attr('class', 'not-valid-add');

        //Обработка таких ссылок
        foreach ($raw_links as $link) {
            if (count($not_valids = $link->find('.not-valid-add')) > 0) {
                continue;
            }
            $link->appendChild($not_valid);
        }

        //Сохраняем статус
        $article->post_content = (string) $document;
        $article->inner_status = 50;
        $article->post_status = 'pending';
        $article->touch();
        $article->save();
    }

    //Выводит список ссылок, которые необходимо обновить
    public function offersToUpdate() {
        $offers = (array) DB::table('offers')
        ->orderBy('parsed', 'asc')
        ->pluck('url')
        ->toArray();
        return implode("\n", $offers);
    }

    //Выводит список категорий и офферов для конкретных сайтов
    public function offersToSite($key) {
        //Проверка
        $site = Site::where("key", "=", $key)->first();
        if (empty($site)) return;

        //Получение списка офферов
        $offers = array();

        //Получение списка категорий
        $categories = (array) DB::table("amazon_categories")
        ->where("site_id", "=", $site->id)
        ->pluck("url")
        ->toArray();

        //Общий список ссылок
        $total = array_merge($offers, $categories);
        return implode("\n", $total);

        $offers = (array) DB::table('offers')
        ->orderBy('parsed', 'asc')
        ->pluck('url')
        ->toArray();
        return implode("\n", $offers);
    }


    //Производит экспорт офферов в WP
    protected function exportOffers($site_key, Request $request) {
        $site = Site::where("key", "=", $site_key)->first();
        if (!$site) abort(404);

        //Ограничители
        $start = $request->get("start");
        if (empty($start)) $start = 0;
        $limit = $request->get("limit");
        if (empty($limit)) $limit = 2000;

        //Прозводим выборку офферов
        $items = (array) DB::table("site_offer_links as Link")
        ->select("Offer.*")
        ->join("offers as Offer", "Link.offer_id", "=", "Offer.id")
        ->where("Link.site_id", "=", $site->id)
        ->whereNotNull("Offer.uniq_link")
        ->skip($start)
        ->limit($limit)
        ->groupBy("Offer.id")
        ->get()
        ->toArray();



    	//Вносим результирующие значения
    	$data = array(
    		"items" => $items
    	);
    	$contents = View::make('wp_export_products', $data);
        return response($contents)->header('Content-Type', 'application/rss+xml');
    }


    //Производит проверку изображений на соответствие тому, чтобы ссылки шли только на текущий домен
    public function checkImages() {
        $ids = $this->fetchArticlesImages();
        $articles = $this->fetchArticleImages($ids);

        foreach ($articles as $article) {
            $this->prepareImagesInvalidOne($article);
        }
    }


    //Список id статей, у которых присутствуют изображения
    protected function fetchArticlesImages() {
        $limit = 500;
        return (array)
        DB::table("articles as Article")
        ->join("sites as Site", "Article.site_id", "=", "Site.id")
        ->where("Article.post_content", "LIKE", "%<img%")
        ->orderBy("Article.post_modified", "ASC")
        ->groupBy("Article.id")
        ->limit($limit) 
        ->pluck("Article.id")
        ->toArray();
    }


    //Список самих статей
    protected function fetchArticleImages($ids) {
        return Article::whereIn("id", $ids)->get();
    }


    //Обрабатывает одну статью
    protected function prepareImagesInvalidOne($article) {
        //Получаем для текущей статьи данные по сайту
        $site_data = (array) DB::table("sites")
        ->where("id", "=", $article->site_id)
        ->pluck("key")
        ->toArray();

        //Проверка на ошибки
        if (empty($site_data) || empty($site_data[0])) {
            $article->touch();
            return;
        }
        $key = $site_data[0];

        //Удаляем старые записи о невалидности изображений
        $content = $article->post_content;
        $document = new Document();
        $document->loadHtml($content);

        /* Это нам не нужно
        $unvalidsx = $document->find('//*[@class="not-valid-image"]', Query::TYPE_XPATH);
        foreach ($unvalidsx as $item) {
            $item->remove();
        }
        */

        //Находим изображения, которые не соответствуют нам
        $imgs = $document->find('//img', Query::TYPE_XPATH);
        $touched = 0;
        foreach ($imgs as $img) {
            $src = $img->attr("src");
            $is_valid = $this->checkSrcValid($src, $key);
            if ($is_valid) continue;

            //Пустой элемент для вставки
            $not_valid = new Element('span', ' (WARNING! Invalid Img Src)');
            $not_valid->setInnerHtml($not_valid->html()."<br/>");
            $not_valid->attr('style', 'color: red; font-weight: bold');
            $not_valid->attr('class', 'not-valid-image');

            //Проверяем есть - ли класс
            $parent = $img->parent();
            if ($parent) {
                $class = $parent->attr("class");
                $class = explode(" ", $class);
                if (in_array("not-valid-image", $class)) {
                    $touched++;
                    continue;
                }
            }

            //Иначе производим обработку
            $not_valid->insertBefore($img);  
            $img->replace($not_valid);
            $touched++;
        }

        //Элемент невалидности
        $newcontent = (string) $document;

        $article->post_content = (string) $document;
        if ($touched > 0) {
            $article->images_invalid = true;
        } else {
            $article->images_invalid = false;
        }
        $article->save();
        $article->touch();
    }


    //Проверка, что все изображение валидно
    protected function checkSrcValid($src, $key) {
        $pattern = "/^https?/";
        $valid = preg_match($pattern, $src, $matches, PREG_OFFSET_CAPTURE);
        if (!$valid) {
            return true;
        } 

        //Проверка уже на валидность самих ссылок (на левые сайты)
        $pattern = "/^https?:\/\/w?w?w?".$key."/";
        $valid = preg_match($pattern, $src, $matches, PREG_OFFSET_CAPTURE);
        if ($valid) {
            return true;
        } else {
            return false;
        }
    }



    //Проверка youtube видеозаписей
    public function checkVideos() {
        $items = $this->getVideoItems();
        foreach ($items as $article) {
            $ids = $this->getVideoIds($article->post_content);

            $count_ids = count($ids);
            $videoList = Youtube::getVideoInfo($ids);
            $count_list = count($videoList);

            //Сохранение
            if ($count_ids != $count_list) {
                $article->videos_invalid = true;
            } else {
                $article->videos_invalid = false;
            }
            $article->videos_invalid_date = now();
            $article->save();
        }
    }


    //Список статей для проверки
    protected function getVideoItems() {
        $limit = 25;
        $articles = Article::limit($limit)
        ->where("post_content", "like", "%<iframe%")
        ->orderBy("videos_invalid_date", "asc")
        ->get();

        return $articles;
    }


    //Получает список id youtube
    protected function getVideoIds($data) {
        $document = new Document();
        $document->loadHtml($data);
        $items = $document->find("//iframe[contains(@src, 'youtube')]", Query::TYPE_XPATH);

        $ids = array();
        foreach ($items as $item) {
            $src = $item->attr("src");

            $videoId = Youtube::parseVidFromURL($src);
            $ids[] = $videoId;
        }

        return $ids;
    }



    //Функция, которая будет производить экспорт данных по статьям в телеграм
    public function sendTelegram() {
        $sites_total = $this->prepareSitesTotal();
        $offers_total = $this->prepareOffersTotal();
        $broken_articles = $this->prepareBrokenArticles();

        //Формируем текст для отправки сообщения
        $view = View::make('telegram_stat', [
            'sites_total' => $sites_total,
            'offers_total' => $offers_total,
            'broken_articles' => $broken_articles
        ]);
        $contents = $view->render();

        //Отправляем сообщение 
        $this->sendTelegramMessage($contents);
    } 


    //Информация по сайтам
    protected function prepareSitesTotal() {
        $data = (array) DB::table("sites as Site")
        ->select(
            "Site.key as key",
            DB::raw('Count(Article.id) as article_count'),
            DB::raw('SUM(Article.char_count) as char_count'), 
            DB::raw('SUM(Article.word_count) as word_count')
        )
        ->leftJoin("articles as Article", "Site.id", "=", "Article.site_id")
        ->groupBy("Site.id")
        ->orderBy("Site.id")
        ->get()
        ->toArray();

        return $data;
    }


    //Информация по офферам
    protected function prepareOffersTotal() {
        $data = (array) DB::table("sites as Site")
        ->select(
            "Site.key as key",
            DB::raw('Count(Offer_link.offer_id) as offer_count')
        )
        ->leftJoin("site_offer_links as Offer_link", "Site.id", "=", "Offer_link.site_id")
        ->groupBy("Site.id")
        ->orderBy("Site.id")
        ->get()
        ->toArray();

        return $data;
    }


    //Информация по сломанным постам
    protected function prepareBrokenArticles() {
        $data = (array) DB::table("articles as Article")
        ->select(
            DB::raw('SUM(IF(Article.images_invalid, 1, 0)) as broken_images'),
            DB::raw('SUM(IF(Article.videos_invalid, 1, 0)) as broken_videos')
        )
        ->get()
        ->toArray();

        return $data;
    }


    //Отправка сообщения на сервер telegram
    protected function sendTelegramMessage($message) {
        $botToken = env('TELEGRAM_TOKEN');
        $chatID = env('TELEGRAM_CHAT');
        $proxy = env('TELEGRAM_PROXY_IP');
        $port = env('TELEGRAM_PROXY_PORT');
        $password = env('TELEGRAM_PROXY_PASSWORD');


        $website="https://api.telegram.org/bot".$botToken;
        $params=[
            'chat_id'=>$chatID,
            'text'=> $message,
        ];
        $ch = curl_init($website . '/sendMessage');

        //Настройка прокси
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_PROXYPORT, $port);
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $password);
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch); 
        curl_close($ch);
    }
}
