<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CustomAdminBuilder;
use App\OfferCategoryLink;
use DB;

class OfferCategory extends Model
{
    use TreeCategoryTrait, CustomAdminBuilder;
    public $timestamps = false;
    protected $fillable = [
        'level', 'text', 'parent'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setParentColumn('parent');
        $this->setOrderColumn('order');
        $this->setTitleColumn('text');
    }

    public function children_one()
    {
        return $this->hasMany(OfferCategory::class, 'parent');
    }


    public function children()
    {
        return $this->children_elements()->with('children');
    }

    public function parentData()
    {
        return $this->hasOne(OfferCategory::class, 'id', 'parent');
    }

    public function allParentData()
    {
        return $this->parentData()->with('allParentData');
    }

    public function category_links_count()
    {
        return $this->hasMany(OfferCategoryLink::class, 'category_id', 'id');
    }

    public function linksCount() {
        return $this->category_links_count()
        ->selectRaw('category_id, count(*) as count')
        ->groupBy('category_id');
    }
}
