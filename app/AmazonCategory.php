<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Site;

class AmazonCategory extends Model
{
    protected $fillable = [
        'name',
        'url'
    ];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
