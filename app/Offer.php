<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SiteOfferLink;
use App\OfferCategoryLink;

class Offer extends Model
{
    protected $casts = [
        'thumbs' => 'array',
        'sizes' => 'array',
        'colors' => 'array',
        'content' => 'array',
    ];

    protected $fillable = [
        'code', 'path', 'title', 'stars', 'images', 'thumbs',
        'price_current', 'price_old', 'price_discount', 'status', 'status_code',
        'sizes', 'colors', 'content', 'parsed', 'url', 'valid', 'uniq_link'
    ];

    //Список связей
    public function site_link()
    {
        return $this->hasMany(SiteOfferLink::class); 
    }

    //Список связи с категориями
    public function category_links()
    {
        return $this->hasMany(OfferCategoryLink::class, 'offer_id', 'id');
    }
}
