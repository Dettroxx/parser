<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferCategoryLink extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'offer_id', 'category_id'
    ];
}
