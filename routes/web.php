<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Для тестового функционала 
Route::get('/ms/{site_id}', 'ExcelExportController@ExportOffersBreadcrumbs'); 

//Для получения списка необработанных офферов
Route::get('/offers-toupdate', 'ExportController@offersToUpdate');
Route::get('/offers-tosite/{key}', 'ExportController@offersToSite');

//Для экспорта на сторону WP
Route::get('/export/offers/{site_key}', 'ExportController@exportOffers');

//Для экспорта в файлы CSV
Route::get('/export/exportCSV_Offers/{site_id}', 'ExcelExportController@ExportOffersBreadcrumbs');